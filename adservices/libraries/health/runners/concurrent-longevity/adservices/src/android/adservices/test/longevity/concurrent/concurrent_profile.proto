// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto2";

package adservices.longevity.profile.concurrent;

option java_package = "android.adservices.test.longevity.concurrent.proto";
option java_multiple_files = true;

message Configuration {
  // Schedule used to run the profile.
  enum Schedule {
    TIMESTAMPED = 1;
    INDEXED = 2;
    SEQUENTIAL = 3;
  }
  optional Schedule schedule = 1 [default = TIMESTAMPED];

  // Information for each scenario.
  message Scenario {
    oneof schedule {
      string at = 1;    // A timestamp (HH:MM:SS) for when to run the scenario.
      int32 index = 2;  // An index for the relative order of the scenario.
    }

    message Journey {
      // Reference to the CUJ (<package>.<class>).
      optional string journey = 1;

      // Optional. Test method name which we want to run. If the test class has single @Test
      // annotated method , this is optional. If multiple @Test annotated methods present, it is
      // must to specify the test method name.
      optional string methodName = 2;

      // Extra arguments to pass to the CUJ.
      message ExtraArg {
        optional string key = 1;
        optional string value = 2;
      }
      repeated ExtraArg extras = 3;
    }

    // Journeys which we want to execute concurrently
    repeated Journey journeys = 3;

    // For app-based scenarios, whether to stay in the app after the tested
    // action is performed.
    enum AfterTest {
      STAY_IN_APP = 1;
      EXIT = 2;
    }
    optional AfterTest after_test = 4 [default = EXIT];
  }
  repeated Scenario scenarios = 2;

  // Amount of times the whole scenarios will be executed.
  // For example, scenarios A->B->C with repetitions = 2 will be executed twice
  // in a row: A->B->C->A->B->C.
  optional int32 repetitions = 3 [default = 1];
}
