// Copyright 2023 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
syntax = "proto3";

package cobalt;

option java_multiple_files = true;
option java_package = "com.google.cobalt";

////////////////////////////////////////////////////////////////////////////////
// NOTE: This file is used by the Cobalt client and the Cobalt servers.
// The source-of-truth of this file is located in Google's internsl code
// repository, and the file is copied to Android where it is used by the Cobalt
// client. Do not edit the copy of this file in this Android repo as those edits
// will be overwritten when the file is next copied.
////////////////////////////////////////////////////////////////////////////////

// A SystemProfile describes the client system on which an Observation is
// collected.
message SystemProfile {
  enum OS {
    UNKNOWN_OS = 0;
    FUCHSIA = 1;
    LINUX = 2;
    ANDROID = 3;
  }

  enum ARCH {
    UNKNOWN_ARCH = 0;
    X86_64 = 1;
    ARM_64 = 2;
    X86 = 3 [deprecated = true];
    X86_32 = 4;
    ARM_32 = 5;
  }

  enum BuildType {
    UNKNOWN_TYPE = 0;
    OTHER_TYPE = 1;
    USER = 2;
    USER_DEBUG = 3;
    ENG = 4;
  }

  OS os = 1;
  ARCH arch = 2;

  // This is a string representing the board name of the device. If a board name
  // cannot be determined, then this field will be 'unknown:<cpu signature>'.
  string board_name = 4;

  // This is a string representing the type of product running on the device.
  //
  // The use of this field is system-specific. For example on Fuchsia it is the
  // experience running on the device, e.g., "smart display" or "workstation".
  string product_name = 5;

  // This is a string representing the version of the currently running system.
  //
  // The use of this field is system-specific. For example, on Fuchsia, it is
  // the build version from fuchsia.buildinfo/Provider, e.g., "0.20200114.1.1".
  // On Android, it is from android.os.Build.VERSION.RELEASE.
  string system_version = 8;

  // This is a string representing the version of the app sending information.
  // There can be multiple apps running on the system and updated separately
  // from the system so sometimes it's more relevant to slice by the app version
  // than the system version above.
  //
  // The use and format of this field is application-specific.
  //
  // The value '<unset>' means the system did not notify Cobalt of the current
  // app_version.
  //
  // The value '<unknown>' means the system explicitly notified Cobalt it did
  // not know the app_version.
  string app_version = 14;

  // This is a string representation of the current channel the device belongs
  // to. It is common to segment a fleet of devices into disjoint populations.
  //
  // The use of this field is system-specific.
  //
  // The value '<unset>' means the system did not notify Cobalt of the current
  // channel.
  //
  // The value '<unknown>' means the system explicitly notified Cobalt it did
  // not know the channel.
  string channel = 9;

  // An enumerated representation of the current build type.
  //
  // The value `UNKNOWN_TYPE` means the system failed to supply a build type.
  // The value `OTHER_TYPE` indicates the system supplied a value that did not
  // match any of the enumerated values.
  BuildType build_type = 11;

  // A list of experiments that are active on the device sorted in increasing
  // order and with no duplicates.
  //
  // This field should only contain experiments specified in the report
  // definition associated with this system profile.
  repeated int64 experiment_ids = 13;

  reserved 3, 6, 7, 10, 12;
  reserved "build_level", "experiments", "realm", "experiment_tokens";
}
