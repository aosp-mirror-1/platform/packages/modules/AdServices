/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.adservices.service.kanon;

import android.content.Context;
import android.content.pm.PackageManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.StrongBoxUnavailableException;

import com.android.adservices.LoggerFactory;
import com.android.internal.annotations.VisibleForTesting;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;

/** Provides method to generate secure hardware key backed X.509 certificate chain */
public class KeyAttestation {

    private static final LoggerFactory.Logger sLogger = LoggerFactory.getFledgeLogger();

    private static final String ANDROID_KEY_STORE = "AndroidKeyStore";

    private static final String PA_KANON_KEY_ALIAS = "PaKanonKeyAttestation";

    private static final String ELLIPTIC_CURVE_KEY_GENERATION_NAME = "secp256r1";

    private final boolean mUseStrongBox;

    private final KeyStore mKeyStore;
    private final KeyPairGenerator mKeyPairGenerator;

    @VisibleForTesting
    KeyAttestation(Boolean useStrongBox, KeyStore keyStore, KeyPairGenerator keyPairGenerator) {
        this.mUseStrongBox = useStrongBox;
        this.mKeyStore = keyStore;
        this.mKeyPairGenerator = keyPairGenerator;
    }

    /** Creates an instance of {@link KeyAttestation} */
    public static KeyAttestation create(Context context)
            throws KeyStoreException, NoSuchAlgorithmException, NoSuchProviderException {
        return new KeyAttestation(
                context.getPackageManager()
                        .hasSystemFeature(PackageManager.FEATURE_STRONGBOX_KEYSTORE),
                KeyStore.getInstance(ANDROID_KEY_STORE),
                KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, ANDROID_KEY_STORE));
    }

    /**
     * Given a challenge, return a {@link KeyAttestationCertificateChainRecord}. The attestation is
     * performed using a 256-bit Elliptical Curve (EC) key-pair generated by the secure hardware.
     */
    public KeyAttestationCertificateChainRecord generateAttestationRecord(final byte[] challenge)
            throws IllegalStateException {
        KeyPair kp = generateHybridKey(challenge, PA_KANON_KEY_ALIAS);
        if (kp == null) {
            sLogger.e("Received a null key pair. Can't generate certificate.");
            throw new IllegalStateException("Unable to generate a key pair");
        }
        return getAttestationRecordFromKeyAlias(PA_KANON_KEY_ALIAS);
    }

    @VisibleForTesting
    KeyPair generateHybridKey(final byte[] challenge, final String keyAlias) {
        try {
            mKeyPairGenerator.initialize(
                    new KeyGenParameterSpec.Builder(
                                    /* keystoreAlias= */ keyAlias,
                                    /* purposes= */ KeyProperties.PURPOSE_SIGN)
                            .setDigests(KeyProperties.DIGEST_SHA256)
                            .setAlgorithmParameterSpec(
                                    new ECGenParameterSpec(ELLIPTIC_CURVE_KEY_GENERATION_NAME))
                            .setAttestationChallenge(challenge)
                            .setIsStrongBoxBacked(mUseStrongBox)
                            .build());

            return mKeyPairGenerator.generateKeyPair();
        } catch (InvalidAlgorithmParameterException e) {
            sLogger.e("Failed to generate EC key for attestation: " + e.getMessage());
            throw new IllegalStateException("Failed to generate a key Pair", e);
        } catch (StrongBoxUnavailableException e) {
            sLogger.e(
                    "Strong box not available on device but isStrongBox is set to true: "
                            + e.getMessage());
            throw new IllegalStateException("Failed to generate a key Pair", e);
        }
    }

    @VisibleForTesting
    KeyAttestationCertificateChainRecord getAttestationRecordFromKeyAlias(String keyAlias) {
        try {
            mKeyStore.load(null);
            Certificate[] certificateChain = mKeyStore.getCertificateChain(keyAlias);
            if (certificateChain == null) {
                throw new IllegalStateException("Certificate chain was null");
            }
            ArrayList<byte[]> attestationRecord = new ArrayList<>();
            for (Certificate certificate : certificateChain) {
                attestationRecord.add(certificate.getEncoded());
            }

            return KeyAttestationCertificateChainRecord.create(attestationRecord);
        } catch (CertificateException
                | IOException
                | KeyStoreException
                | NoSuchAlgorithmException e) {
            sLogger.e("Exception when" + "loading certs for attestation: " + e.getMessage());
            return KeyAttestationCertificateChainRecord.create(new ArrayList<>());
        }
    }
}
