/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.adservices.service.measurement.access;

import static android.adservices.common.AdServicesStatusUtils.STATUS_UNSET;

public final class AccessInfo {
    private final boolean mIsAllowedAccess;
    private final int mResponseCode;

    public AccessInfo(boolean isAllowedAccess) {
        this(isAllowedAccess, STATUS_UNSET);
    }

    public AccessInfo(boolean isAllowedAccess, int responseCode) {
        mIsAllowedAccess = isAllowedAccess;
        mResponseCode = responseCode;
    }

    public boolean isAllowedAccess() {
        return mIsAllowedAccess;
    }

    public int getResponseCode() {
        return mResponseCode;
    }
}
