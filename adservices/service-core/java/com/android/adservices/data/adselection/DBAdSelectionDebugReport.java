/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.adservices.data.adselection;

import android.annotation.NonNull;
import android.annotation.Nullable;
import android.net.Uri;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.auto.value.AutoValue;

import java.time.Instant;
import java.util.Objects;

/** POJO for the debug reports generated during an ad selection.. */
@AutoValue
@AutoValue.CopyAnnotations
@Entity(
        tableName = DBAdSelectionDebugReport.TABLE_NAME,
        indices = {@Index(value = {"creation_timestamp"})})
public abstract class DBAdSelectionDebugReport {

    public static final String TABLE_NAME = "ad_selection_debug_report";

    /** Gets the autogenerated ad selection debug report id. */
    @AutoValue.CopyAnnotations
    @ColumnInfo(name = "ad_selection_debug_report_id")
    @PrimaryKey(autoGenerate = true)
    @Nullable
    public abstract Long getAdSelectionDebugReportId();

    /** Gets the debug report uri. */
    @AutoValue.CopyAnnotations
    @ColumnInfo(name = "debug_report_uri")
    @NonNull
    public abstract Uri getDebugReportUri();

    /** Gets the dev options enabled flag. */
    @AutoValue.CopyAnnotations
    @ColumnInfo(name = "dev_options_enabled")
    @NonNull
    public abstract boolean getDevOptionsEnabled();

    /** Gets the creation timestamp. */
    @AutoValue.CopyAnnotations
    @ColumnInfo(name = "creation_timestamp")
    @NonNull
    public abstract long getCreationTimestamp();

    /** Returns an AutoValue builder for a {@link DBAdSelectionDebugReport} object. */
    @NonNull
    public static Builder builder() {
        return new AutoValue_DBAdSelectionDebugReport.Builder()
                .setAdSelectionDebugReportId(null)
                .setCreationTimestamp(Instant.now().toEpochMilli());
    }

    /** creates a DBAdSelectionDebugReport object. */
    @NonNull
    public static DBAdSelectionDebugReport create(
            @Nullable Long adSelectionDebugReportId,
            @NonNull Uri debugReportUri,
            boolean devOptionsEnabled,
            long creationTimestamp) {
        Objects.requireNonNull(debugReportUri);
        return builder()
                .setAdSelectionDebugReportId(adSelectionDebugReportId)
                .setDebugReportUri(debugReportUri)
                .setDevOptionsEnabled(devOptionsEnabled)
                .setCreationTimestamp(creationTimestamp)
                .build();
    }

    /** Builder class for a {@link DBAdSelectionDebugReport} object. */
    @AutoValue.Builder
    public abstract static class Builder {
        /**
         * Sets the numerical ID linking {@link DBAdSelectionDebugReport}.d.
         *
         * <p>This ID is only used internally and does not need to be stable or reproducible. It is
         * auto-generated by Room if set to {@code null} on insertion.
         */
        @NonNull
        public abstract Builder setAdSelectionDebugReportId(
                @Nullable Long adSelectionDebugReportId);

        /** Sets the debug report Uri. */
        @NonNull
        public abstract Builder setDebugReportUri(@NonNull Uri debugReportUri);

        /** Sets if the dev options are enabled. */
        @NonNull
        public abstract Builder setDevOptionsEnabled(boolean devOptionsEnabled);

        /** Sets the creation timestamp. */
        @NonNull
        public abstract Builder setCreationTimestamp(long creationTimestamp);

        /**
         * Builds and returns the {@link DBAdSelectionDebugReport} object.
         *
         * @throws IllegalStateException if any required field is unset when the object is built
         */
        @NonNull
        public abstract DBAdSelectionDebugReport build();
    }
}
