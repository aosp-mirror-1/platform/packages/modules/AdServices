<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2023 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<androidx.appcompat.widget.LinearLayoutCompat
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/apps_fragment"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <androidx.core.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:scrollbars="vertical"
        android:focusableInTouchMode="true" >
        <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">
            <View
                android:id="@+id/adservices_ppapi_title_spacing"
                android:visibility="@integer/adservices_ppapi_title_spacing"
                android:layout_height="@dimen/adservices_ppapi_title_spacing_height"
                android:layout_width="match_parent"/>
            <TextView
                android:id="@+id/apps_ga_introduction"
                android:layout_marginTop="@dimen/adservices_content_three_quarter_padding"
                android:paddingHorizontal="@dimen/adservices_content_horizontal_padding"
                android:text="@string/settingsUI_apps_view_ga_subtitle"
                style="@style/MainStyle.Body2"/>

            <com.android.settingslib.widget.MainSwitchBar
                android:id="@+id/apps_switch_bar"
                android:layout_height="wrap_content"
                android:layout_width="match_parent"
                android:title="@string/settingsUI_apps_switch_title"
                android:textDirection="locale"/>
            <LinearLayout
                android:id="@+id/no_apps_message"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/adservices_empty_list_margin_top"
                android:layout_marginBottom="@dimen/adservices_empty_list_margin_bottom"
                android:gravity="center"
                android:focusable="false"
                android:visibility="gone"
                android:orientation="vertical">
                <ImageView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:src="@drawable/ic_placeholder_icon_for_empty_apps_list"
                    android:focusable="false"
                    android:importantForAccessibility="no"
                    android:contentDescription="@string/ic_placeholder_icon_for_empty_apps_list_description" />
                <TextView
                    android:id="@+id/no_apps_state"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/adservices_empty_list_margin_top"
                    android:paddingHorizontal="@dimen/adservices_content_horizontal_padding"
                    android:gravity="center"
                    android:text="@string/settingsUI_apps_view_no_apps_text"
                    android:textAppearance="@style/TextAppearance.Body2.Bold" />
                <Button
                    android:id="@+id/blocked_apps_when_empty_state_button"
                    style="@style/MainStyle.PrimaryButton"
                    android:layout_marginTop="@dimen/adservices_empty_state_btn_margin"
                    android:text="@string/settingsUI_blocked_apps_title"/>
            </LinearLayout>

            <androidx.recyclerview.widget.RecyclerView
                android:id="@+id/apps_list"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/adservices_empty_state_btn_margin"
                android:clipToPadding="false"
                android:paddingHorizontal="@dimen/adservices_content_horizontal_padding"
                android:scrollbars="none" />

            <LinearLayout
                android:id="@+id/empty_apps_hidden_section"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:paddingBottom="@dimen/adservices_content_padding" >
                <LinearLayout
                    android:id="@+id/blocked_apps_button"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal"
                    android:padding="@dimen/adservices_content_padding"
                    android:background="?android:attr/selectableItemBackground" >
                    <TextView
                        android:id="@+id/blocked_apps_button_child"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:key="blocked_apps"
                        android:text="@string/settingsUI_blocked_apps_ga_title"
                        android:textAppearance="@style/TextAppearance.H5"
                        android:drawableStart="@drawable/ic_chevron_right_24dp"
                        android:drawablePadding="@dimen/adservices_drawable_padding"/>
                </LinearLayout>
                <LinearLayout
                    android:id="@+id/reset_apps_button"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal"
                    android:padding="@dimen/adservices_content_padding"
                    android:background="?android:attr/selectableItemBackground" >
                    <TextView
                        android:id="@+id/reset_apps_button_child"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:key="reset_apps"
                        android:text="@string/settingsUI_reset_apps_title"
                        android:textAppearance="@style/TextAppearance.H5"
                        android:drawableStart="@drawable/ic_restart_alt_24dp"
                        android:drawablePadding="@dimen/adservices_drawable_padding"/>
                </LinearLayout>

                <LinearLayout
                    android:id="@+id/apps_view_ga_footer"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:paddingHorizontal="@dimen/adservices_content_horizontal_padding"
                    android:paddingBottom="@dimen/adservices_content_2x_padding"
                    android:orientation="vertical"
                    android:focusableInTouchMode="true">
                    <ImageView
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/adservices_footer_content_half_padding"
                        android:layout_marginBottom="@dimen/adservices_footer_content_padding"
                        android:src="@drawable/ic_info_icon"
                        tools:ignore="ContentDescription" />
                    <TextView
                        android:id="@+id/apps_view_ga_footer_text"
                        android:text="@string/settingsUI_apps_view_footer"
                        style="@style/MainStyle.Body2" />
                </LinearLayout>
            </LinearLayout>
        </LinearLayout>
    </androidx.core.widget.NestedScrollView>
</androidx.appcompat.widget.LinearLayoutCompat>
