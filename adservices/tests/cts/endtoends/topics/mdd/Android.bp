// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_test {
    name: "CtsAdServicesMddTests",
    team: "trendy_team_android_rubidium",
    srcs: [
        "src/**/*.java",
    ],
    static_libs: [
        "androidx.concurrent_concurrent-futures",
        "compatibility-device-util-axt", // Needed for ShellUtils.runShellCommand
        "truth",
        "adservices-clients",
        "adservices-test-utility",
    ],
    libs: [
        "android.test.base.stubs.system",
        "framework-adservices.stubs.module_lib",
        "framework-sdksandbox.impl",
    ],
    test_suites: [
        "cts",
        "general-tests",
        "mts-adservices",
        "mcts-adservices",
    ],
    sdk_version: "module_current",
    min_sdk_version: "Tiramisu",
    lint: {
        extra_check_modules: ["AdServicesTestLintChecker"],
        test: false, // TODO(b/343741206): remove when checks will run on android_test
    },
}

android_test {
    name: "CtsAdExtServicesMddTests",
    srcs: [
        "src/**/*.java",
    ],
    static_libs: [
        "androidx.concurrent_concurrent-futures",
        "compatibility-device-util-axt", // Needed for ShellUtils.runShellCommand
        "truth",
        "adservices-clients",
        "adservices-test-utility",
    ],
    libs: [
        "android.ext.adservices",
        "android.test.base.stubs.system",
        "framework-adservices.stubs.module_lib",
        "framework-sdksandbox.impl",
    ],
    test_suites: [
        "cts",
        "general-tests",
        "mts-extservices",
        "mcts-extservices",
    ],
    sdk_version: "module_current",
    min_sdk_version: "31",
    max_sdk_version: "32",
    test_config: "AndroidTest.ExtServices.xml",
    manifest: "AndroidManifestExtServices.xml",
    lint: {
        extra_check_modules: ["AdServicesTestLintChecker"],
        test: false, // TODO(b/343741206): remove when checks will run on android_test
    },
}
