<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (C) 2024 The Android Open Source Project
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
<configuration description="Config for Cts Ad Services Custom Audience tests in ExtServices">
    <option name="test-suite-tag" value="cts" />
    <option name="test-tag" value="CtsAdExtServicesCustomAudienceTests" />
    <option name="config-descriptor:metadata" key="component" value="adservices"/>
    <option name="config-descriptor:metadata" key="parameter" value="not_instant_app" />
    <option name="config-descriptor:metadata" key="parameter" value="not_multi_abi" />
    <option name="config-descriptor:metadata" key="parameter" value="secondary_user" />
    <!-- Needed for correctly being picked up in presubmit -->
    <option name="config-descriptor:metadata" key="mainline-param" value="com.google.android.extservices.apex" />


    <!-- Prevent test from running on Android T+ -->
    <object type="module_controller"
        class="com.android.tradefed.testtype.suite.module.MaxSdkModuleController">
        <option name="max-sdk-level" value="32"/>
    </object>

    <!-- Prevent tests from running on Android R- -->
    <object type="module_controller"
        class="com.android.tradefed.testtype.suite.module.Sdk31ModuleController"/>

    <!-- Only run on devices containing the ExtServices module -->
    <object type="module_controller" class="com.android.tradefed.testtype.suite.module.MainlineTestModuleController">
        <option name="mainline-module-package-name" value="com.google.android.extservices"/>
    </object>

    <target_preparer class="com.android.tradefed.targetprep.suite.SuiteApkInstaller">
        <option name="cleanup-apks" value="true"/>
        <!-- The test apps to be installed and uninstalled -->
        <option name="test-file-name" value="CtsAdExtServicesCustomAudienceTests.apk"/>
    </target_preparer>

    <test class="com.android.tradefed.testtype.AndroidJUnitTest">
        <option name="hidden-api-checks" value="false" /> <!-- Allow hidden API uses -->
        <option name="package" value="com.android.adextservices.tests.cts.customaudience"/>
    </test>

    <target_preparer class="com.android.tradefed.targetprep.RunCommandTargetPreparer">
        <!-- Disable GPP flagging test apps -->
        <option name="run-command" value="settings put global verifier_engprod 1" />
        <option name="teardown-command" value="settings put global verifier_engprod 0" />

        <!-- Create place to store tests apks that will be installed/uninstalled in the test. -->
        <option name="run-command" value="mkdir -p /data/local/tmp/cts/install" />
        <option name="teardown-command" value="rm -rf /data/local/tmp/cts"/>

        <!-- Uninstall the test apps anyway in case failed test skips the uninstallation -->
        <option name="test-user-token" value="%TEST_USER%"/>
        <option name="teardown-command"
            value="pm uninstall --user %TEST_USER% com.android.adservices.tests.cts.customaudience.sampleapphighbid" />
        <option name="teardown-command"
            value="pm uninstall --user %TEST_USER% com.android.adservices.tests.cts.customaudience.sampleapplowbid" />
    </target_preparer>

    <!-- Push compiled APK file of test apps to a local place for installation -->
    <target_preparer class="com.android.compatibility.common.tradefed.targetprep.FilePusher">
        <option name="push"
            value="CtsSampleCustomAudienceAppHighBid.apk->/data/local/tmp/cts/install/CtsSampleCustomAudienceAppHighBid.apk" />
        <option name="push"
            value="CtsSampleCustomAudienceAppLowBid.apk->/data/local/tmp/cts/install/CtsSampleCustomAudienceAppLowBid.apk" />
    </target_preparer>

    <object type="module_controller"
        class="com.android.tradefed.testtype.suite.module.MainlineTestModuleController">
        <option name="mainline-module-package-name" value="com.google.android.adservices"/>
    </object>
    <option name="config-descriptor:metadata" key="mainline-param" value="com.google.android.adservices.apex" />
</configuration>
