# Problem
On Android S, AdServices code is part of the ExtServices APK. During normal functioning of AdServices, it generates multiple data files (e.g., adservices.db, adservices-msmt.db etc.). These files are stored in the APK’s data directory for the current user (e.g., /data/user/0/com.google.android.ext.services/).

When the device receives an OTA from S to T, the AdServices apex gets installed and the AdServices code gets removed from the ExtServices APK on T. After OTA, the consent state, blocked-topics, and blocked-apps are migrated because they’re stored in AppSearch. The data in the APK’s directory is not migrated, and the PPAPIs within AdServices behave as if it’s a fresh user.

User data within AdServices is cleaned up by scheduled jobs that run periodically. However, because the AdServices code is removed from the ExtServices APK, it does not clean up all the files that were generated during normal operation before the OTA. It therefore results in multiple orphaned files within the ExtServices APK post-OTA. Some of these files contain user data and must be deleted.

There is also non-AdServices code within ExtServices, and those could be creating files within the data directory as well. These files must be preserved post-OTA and should not be deleted.

# High-Level Strategy
After an OTA to T, we need to do the following:
1. Identify the AdServices files within the ExtServices data directory
2. Trigger code within ExtServices that is responsible for cleaning up the orphaned files.
3. Record successful deletion, and do not trigger this cleanup code subsequently.

Edge cases to also consider:
1. New files may be added to the AdServices code over the next few years that would also need to be cleaned up if the device OTAs from S running the new AdServices version.
2. Any manual identification of the generated files may miss something, for which we’ll need to be able to update the cleanup logic and execute it again.

# Identifying AdServices files
To distinguish between files generated by AdServices and non-AdServices code in ExtServices, we’ll rename all the files being created to have a prefix of adservices. In some cases, the files already contain this prefix, so those can be left as-is. Files that don’t contain this prefix will be renamed only on R/S to instead start with adservices_. We cannot rename or change the location of current files on T+ since that would break existing functionality.

# What will be deleted
1. Any folder that begins with adservices will be deleted completely (i.e., all files, recursive).
2. Any file in any folder that begins with adservices, even if the folder name does not have adservices in it.
## Examples of files that will be deleted:
* /data/user/0/com.google.android.ext.services/files/adservices.db
* /data/user/0/com.google.android.ext.services/files/nested/adservices.db
* /data/user/0/com.google.android.ext.services/databases/adservices-data/fledge.db
* /data/user/0/com.google.android.ext.services/cache/adservices_cache/nested/temp.xml
## Examples of files that will NOT be deleted:
* /data/user/0/com.google.android.ext.services/files/textclassifier.xml
* /data/user/0/com.google.android.ext.services/databases/textclassifier/model.db

# Preventing future issues
The biggest risk comes from engineers accidentally not following the naming conventions for new files that will be added in the future. In order to minimize the risk of that happening, all new files and databases should be added to a sub-folder named adservices-data within each of the top-level folders (e.g., .../files/adservices-data/ and .../databases/adservices-data/).

To enforce this automatically, we have created this lint rule that prohibits the direct use of context.getFilesDir(), context.getDatabasePath() and Room.databaseBuilder. We should instead use helper utility classes that will ensure that the location of the files & databases lie within the adservices-data sub-folder.
