Contains artifacts that are shared between multiple AdServices-related projects
such as:
- AdServices system_server components
- AdServices service
- SDK Sandbox
- On-Device Personalization
