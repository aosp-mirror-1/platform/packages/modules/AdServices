/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.adservices.mockito;

import com.android.adservices.shared.SharedUnitTestCase;
import com.android.adservices.shared.meta_testing.TestNamerRuleTester;

import org.junit.Test;

public final class AdServicesExtendedMockitoRuleTest extends SharedUnitTestCase {

    @Test
    public void testGetName() throws Throwable {
        AdServicesExtendedMockitoRule rule = new AdServicesExtendedMockitoRule.Builder().build();
        TestNamerRuleTester<AdServicesExtendedMockitoRule> tester =
                new TestNamerRuleTester<>(expect, rule);

        tester.justDoIt();
    }

    // TODO(b/339831452): add unit tests for other functionalities
}
